using System;
using Xunit;
using Calculator;

namespace CalculatorTests
{
    public class UnitTest1
    {
        [Fact]
        public void Addition_AddTwoIntegers_ShouldReturnSum()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 1;
            int rhs = 1;
            int expected = lhs + rhs;
            //Act
            int actual = calculator.Addition(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Subtraction_SubtractRhsFromLhs_ShoulReturnSubtraction()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 1;
            int rhs = 1;
            int expected = lhs - rhs;
            //Act
            int actual = calculator.Subtraction(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Multiply_MultiplyLhsWithRhs_ShouldReturnMultiply()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 2;
            int rhs = 2;
            int expected = 2 * 2;
            //Act
            int actual = calculator.Multiply(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Divide_DivideLhsWithRhs_ShouldReturnDivision()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 2;
            int rhs = 2;
            int expected = lhs / rhs;
            //Act
            int actual = calculator.Divide(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Divide_DivideByZero_ShouldReturnException()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 1;
            int rhs = 0;
            //Act & Assert
            Assert.Throws<DivideByZeroException>(()=>calculator.Divide(lhs, rhs));
        }
        [Fact]
        public void Remainder_DivideLhsWithRhs_ShouldReturnRemainder()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 7;
            int rhs = 2;
            int expected = lhs % rhs;
            //Act
            int actual = calculator.Remainder(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Remainder_DivideByZero_ShouldReturnException()
        {
            //Arrange
            Calculatorr calculator = new Calculatorr();
            int lhs = 7;
            int rhs = 0;
            //Act & Assert
            Assert.Throws<DivideByZeroException>(() => calculator.Remainder(lhs, rhs));
        }
    }
}
