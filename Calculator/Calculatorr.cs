﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class Calculatorr
    {
        public int Addition(int num1, int num2)
        {
            int result = num1 + num2;
            return result;
        }
        public int Subtraction(int num1, int num2)
        {
            int result = num1 - num2;
            return result;
        }
        public int Multiply(int num1, int num2)
        {
            int result = num1 * num2;
            return result;
        }
        public int Divide(int num1, int num2)
        {
            if(num2 != 0)
            {
                int result = num1 / num2;
                return result;
            }
            else
            {
                throw new DivideByZeroException();
            }
        }
        public int Remainder(int num1, int num2)
        {
            if(num2 != 0)
            {
                int result = num1 % num2;
                return result;
            }
            else
            {
                throw new DivideByZeroException();
            }
        }
    }
}
