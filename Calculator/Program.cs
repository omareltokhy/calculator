﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool calc = true;
            Calculatorr calcu = new Calculatorr();

            do
            {
                Console.WriteLine("Give a number: ");
                int firstNumber = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Choose a function (+ / - / * / / / %): ");
                string func = Console.ReadLine();
                Console.WriteLine("Choose a second number: ");
                int secondNumber = Convert.ToInt32(Console.ReadLine());

                if (func == "+") { Console.WriteLine(calcu.Addition(firstNumber, secondNumber)); }
                if (func == "-") { Console.WriteLine(calcu.Subtraction(firstNumber, secondNumber)); }
                if (func == "*") { Console.WriteLine(calcu.Multiply(firstNumber, secondNumber)); }
                if (func == "/") { Console.WriteLine(calcu.Divide(firstNumber, secondNumber)); }
                if (func == "%") { Console.WriteLine(calcu.Remainder(firstNumber, secondNumber)); }

                Console.WriteLine("Do you want to stop?(y/n)");
                string stop = Console.ReadLine();
                if(stop == "y") { calc = false; }
            }
            while (calc == true);
        }
    }
}
